int numLines, cols;
int baseCol = floor(random(30,60));

int[] colors = {
  baseCol+floor(random(0,10)),
  baseCol+floor(random(10,20)),
  baseCol-floor(random(0,10)),
  baseCol-floor(random(10,20)),
};

void setup () {
  size (800, 800);
  noFill();
  numLines = width/2;  
  cols = width/numLines;
  colorMode(HSB,100,100,100,100);
}

void draw () {
  background(0);
  noiseDetail(10, 0.5);
  for (int x = 0; x < width; x+=cols) {
    float n = noise(x, frameCount*0.007);
    int col = floor(map(n,0,1,0,4));
    stroke(colors[col],40,100,255 * n);
    line(x, 0, x, height - n * height);
  }
}
